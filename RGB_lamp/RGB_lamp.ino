#include <ESP8266WiFi.h>
#include <Arduino.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Adafruit_NeoPixel.h>
#include <FS.h>
#include <Wire.h>
#include <DHT.h>
#include "LittleFS.h"

#define PIN D6

#define NUMPIXELS 60

#define DHTPIN D1
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE); 

const char *ssid = "3301-IoT";
const char *password = "mikrobus";

const int led_deska = LED_BUILTIN;
const int foto_pin = A0;
float vlhkost = 0;
float teplota = 0;
int foto_hodnota = 0;


char* ledState;

AsyncWebServer server(80);

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
volatile uint8_t mode = 0;
volatile uint8_t red = 0;
volatile uint8_t green = 0;
volatile uint8_t blue = 0;

String getFoto() {
  foto_hodnota = analogRead(foto_pin);
  Serial.println(foto_hodnota);
  return String(foto_hodnota);
}

String getVlhkost() {
  vlhkost = dht.readHumidity();
  Serial.println(vlhkost);
  return String(vlhkost);
  
}

String getTeplota() {
  teplota = dht.readTemperature();
  Serial.println(teplota);
  return String(teplota);
  
}

String processor(const String& var){
  Serial.println(var);
  if(var == "FOTO"){
    return String(foto_hodnota);
  }

  else if(var == "VLHK"){
    return String(vlhkost);
  }
  
  else if(var == "TEPL"){
    return String(teplota);
  }
  return "";
}

void setup(){
  dht.begin();
  pixels.begin();
  Serial.begin(115200);  
  pinMode(led_deska, OUTPUT);
  digitalWrite(led_deska, LOW);

  if(!LittleFS.begin()){
    Serial.println("Při připojování LittleFS vznikla chyba");
    return;
  }

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  Serial.println("");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("\nPřipojeno na: ");
  Serial.println(ssid);
  Serial.print("IP adresa: ");
  Serial.println(WiFi.localIP());

  // if (MDNS.begin("esp8266")) {Serial.println("MDNS responder started");}

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(LittleFS, "/index.html", String(), false, processor);
  });

  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(LittleFS, "/style.css", "text/css");
  });

    server.on("/1", HTTP_GET, [](AsyncWebServerRequest *request){
    mode = 1;
    request->redirect("/");
  });
  
  server.on("/2", HTTP_GET, [](AsyncWebServerRequest *request){
    mode = 2;
    request->redirect("/");
  });
  
  server.on("/3", HTTP_GET, [](AsyncWebServerRequest *request){
    mode = 3;
    request->redirect("/");
  });
  server.on("/setColor", HTTP_GET, [&pixels](AsyncWebServerRequest *request){
    mode=0;
    red = request->getParam("red")->value().toInt();
    green = request->getParam("green")->value().toInt();
    blue = request->getParam("blue")->value().toInt();   
    request->redirect("/");
  });

  server.on("/foto", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(foto_hodnota).c_str());
  });

  server.on("/vlhk", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(vlhkost).c_str());
  });

  server.on("/tepl", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", String(teplota).c_str());
  });

  server.begin();
}

void mode1(){
    for(int i=0;i<NUMPIXELS;i++){
      if (i % 4 == 0) {getFoto();
       getTeplota();
       getVlhkost();
       };
    pixels.setPixelColor(i,pixels.Color(red,green,blue));
    pixels.show();
    delay(250);
    }
    for(int i=0; i<NUMPIXELS;i++){
      if (i % 4 == 0) {getFoto();
       getTeplota();
       getVlhkost();
       };
       pixels.setPixelColor(i,pixels.Color(0,0,0));
        pixels.show();
    delay(250);
    }
 }
 void mode2(){
    
   int redRand = random(128);
   int greenRand = random(128);
   int blueRand = random(128);
   for(int i=0;i<NUMPIXELS;i++){
    if (i % 4 == 0) {getFoto();
       getTeplota();
       getVlhkost();
       };
    pixels.setPixelColor(i,pixels.Color(redRand,greenRand,blueRand));
    pixels.show();
   }
   delay(250);
 }
 void mode3(){
  for(int i=0;i<NUMPIXELS;i++){
    if (i % 4 == 0) {getFoto();
       getTeplota();
       getVlhkost();
       };
    pixels.setPixelColor(i,pixels.Color(red,green,blue));
    pixels.show();
    }
    delay(300);
    for(int i=0;i<NUMPIXELS;i++){
      if (i % 4 == 0) {getFoto();
       getTeplota();
       getVlhkost();
       };
    pixels.setPixelColor(i,pixels.Color(0,0,0));
    pixels.show();
    }
    delay(300);
 }
 
void loop(){
  getTeplota();
  getVlhkost();
  getFoto();

  switch(mode){
    case 0 :   
      for(int i=0;i<NUMPIXELS;i++){
      pixels.setPixelColor(i,pixels.Color(red,green,blue));
      pixels.show();
      }
      break;
    case 1 :
      pixels.clear();
      mode1();
      break;
    case 2 : 
      pixels.clear();
      mode2();
      break;
    case 3 :
      pixels.clear();
      mode3();
      break;
  }
  delay(1500);
}
